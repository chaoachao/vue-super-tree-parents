# vue2-ztree

> vue2-ztree: ：基于[ztree](https://treejs.cn/v3/main.php#_zTreeInfo)封装的 Vue2.x 树形组件，轻松实现海量数据的高性能渲染。

![logo](css/demoImg/demo1.png)
![logo](css/demoImg/demo2.png)


vue2-ztree 仅仅是给 ztree3.0 套了一层 Vue 组件的壳。

ztree 在性能优化方面已经做到了近乎极致，感谢 ztree 作者的工作，向您致敬！

## 安装

```bash
npm i vue2-ztree --save
```

## 使用

in script:

```javascript
import { vueTree } from 'vue2-ztree'

export default {
	components: {
        vueTree
	},
	data() {
		return {    
                    searchFrom:{
                        name:'',
                    },
                    treeData:[
                        {
                            id:'1',
                            pId:'',
                            name:'祖父'
                        },
                        {
                            id:'2',
                            pId:'1',
                            name:'父亲'
                        },
                        {
                            id:'3',
                            pId:'1',
                            name:'叔叔'
                        },
                        {
                            id:'4',
                            pId:'2',
                            name:'儿子'
                        },
                        {
                            id:'5',
                            pId:'3',
                            name:'表妹'
                        },
                        {
                            id:'6',
                            pId:'3',
                            name:'表弟'
                        },
                        {
                            id:'7',
                            pId:'4',
                            name:'孙子'
                        },
                        {
                            id:'7',
                            pId:'4',
                            name:'孙子'
                        },
                        {
                            id:'8',
                            pId:'1',
                            name:'大姑'
                        },
                        {
                            id:'9',
                            pId:'8',
                            name:'表哥'
                        },
                    ],
		}
	},
    created() {
        this.$refs.tree2.setData(this.treeData)
    },
    methods: {
        nodeClick(node) {
            this.searchFrom.name = node.name
        },
        checkChange(node) {
            this.searchFrom.name = node.lables.join()
        },
    }
	...
```

in template:

```html
 <vue-tree ref="tree2" is-check :include-parent="false" open-all @nodeClick="nodeClick" @checkChange="checkChange" />
```

## 属性

| 参数           | 说明       | 类型   | 默认值                      |
| -------       | ---------- | ------ | --------------------------- |
| setting       | ztree 配置 | Object | `{view: {showIcon: false}}` |
| treeId        | 对应的treeId | string  | 'treeDemo'                   |
| hasSearch     | 是否有筛选输入框 | Boolean  | false                        |
| emptyText     | 空数据时 提示语 | string  | '暂无数据'                     |
| includeParent | 多选是否包含 组节点 | Boolean  | false                       |
| isCheck       | 是否是多选 | Boolean  | false                    |
| openAll       | 是否全部展开 | Boolean  | false                    |
| dataStructure   | 简单数据模式（直接传数据列表组件根据父子关机转换成对应json数据） 父子对应的数据id  idKey(子id) pIdKey(父id) rootPId(用于修正根节点父节点数据，即 pIdKey 指定的属性值。非必填) | Object  |    {idKey: 'id', pIdKey: 'pId',}                 |

## 事件

完全移植[zTree API](https://treejs.cn/v3/api.php)事件

## $emit事件方法
| 事件名称       | 说明                                              |
| -------------- | ------------------------------------------------- |
| nodeClick      | 用于捕获节点被点击的事件回调函数|
| checkChange    | 用于捕获 checkbox / radio 被勾选 或 取消勾选的事件回调函数|

## 向外暴露方法
| 方法名称       | 说明                                              |
| -------------- | ------------------------------------------------- |
| dataEcho       | 用于数据回显                                            |
| setData        | 重新设置 树结构列表数据                       |

## 扩展 
若 前端UI框架时 element-ui 
from 表单中使用时 结合 el-popover 使用
可参考一下代码

```html
 <el-form-item label="审批人">
    <el-popover placement="bottom" width="265" trigger="click" @show="treeShow">
        <vue-tree ref="tree2" ischeck :include-parent="false" @nodeClick="nodeClick" @checkChange="checkChange" />
        <el-input
                slot="reference"
                v-model="formInline.user"
                :disabled="true"
                placeholder="请选择审批人"
                size="mini"
        >请选择审批人
        </el-input>
    </el-popover>
</el-form-item>
//注意给 pop加高度
<style >
    .el-popover{
        height: 200px;
        overflow: auto;
    }

</style>

```

mc 2022109